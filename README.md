# Metalearned Neural Memory #

This repo contains a sample Pytorch code accompanying the paper, [Metalearned Neural Memory (Munkhdalai et al., NeurIPS 2019)](https://arxiv.org/abs/1907.09720).

Prerequisites
-------------

- Pytorch 1.0 or latest
- Other data utils: sklearn, numpy etc.


Usage
-----
To run the code for the programming tasks, see the usage instructions in `pt_train_dictinf.py`.


Author
------
Tsendsuren Munkhdalai / [@tsendeemts](http://www.tsendeemts.com/)
