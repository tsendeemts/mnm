import time

import numpy as np

from torch.autograd import Variable
import torch
import torch.nn as nn
import torch.nn.functional as F

from FFMemoryLearned import FFMemoryLearned

class MNMp(nn.Module):

	def __init__(self, n_units, n_in_mem = 50, n_units_mem = 150, n_batch_mem=16, layer_norm_kv=False):
		if n_in_mem is None:
			n_in_mem = n_units
		if n_units_mem is None:
			n_units_mem = n_units
		super(MNMp, self).__init__()

		self.lstm_l1 = nn.LSTMCell(n_units+n_in_mem, n_units)
		self.heads_l2 = nn.Linear(n_units, (n_batch_mem*3+1)*n_in_mem)
		self.memfunc = FFMemoryLearned(n_in = n_in_mem, n_out = n_in_mem, n_units = n_units_mem, h_nonlinty='tanh')
		self.f_l = nn.Linear(n_in_mem, 1)
		self.read_out = nn.Linear(n_units+n_in_mem, n_units)

		self.layer_norm = nn.LayerNorm((n_batch_mem*3+1)*n_in_mem)

		self.n_batch_mem = n_batch_mem
		self.n_in_mem = n_in_mem
		self.n_units_mem = n_units_mem
		self.n_units = n_units
		self.h = None
		self.replay_k = []
		self.replay_v = []
		self.layer_norm_kv = layer_norm_kv

		self.h_lstm = None
		self.c_lstm = None

	def unchain_state(self):
		if self.h is not None:
			self.h = self.h.detach()
			self.h_lstm = self.h_lstm.detach()
			self.c_lstm = self.c_lstm.detach()

		self.replay_k[:] = []
		self.replay_v[:] = []
		
		self.memfunc.unchain_state()

	def reset_state(self):
		self.h = None
		self.replay_k[:] = []
		self.replay_v[:] = []
		self.memfunc.reset_mem()

		self.h_lstm = None
		self.c_lstm = None

		
	def forward(self, x):
		if self.h is None:
			self.h = torch.zeros((x.shape[0], self.n_in_mem)).cuda().float()
			self.h_lstm = torch.zeros((x.shape[0], self.n_units)).cuda().float()
			self.c_lstm = torch.zeros((x.shape[0], self.n_units)).cuda().float()

		self.h_lstm, self.c_lstm = self.lstm_l1(torch.cat([x, self.h], dim=1), (self.h_lstm, self.c_lstm))
		if self.layer_norm_kv:
			h = self.layer_norm(self.heads_l2(self.h_lstm))
		else:
			h = torch.tanh(self.heads_l2(self.h_lstm))
		betta, n_k_v = torch.split(h, [self.n_in_mem, h.shape[1] - self.n_in_mem], dim=1)
		betta = torch.sigmoid(self.f_l(betta))
		
		n_k_v = n_k_v.view(n_k_v.shape[0], self.n_batch_mem, -1).contiguous()
		k_w, v_w, k_r = torch.chunk(n_k_v, 3, dim=2)
		
		re_const_loss, re_const_loss_init = self.memfunc.update(k_w, v_w, f_gate=betta)
		
		self.h = self.memfunc.read(k_r)
		
		h_lstm = self.read_out(torch.cat([self.h_lstm, self.h], dim=1))

		return h_lstm, re_const_loss, re_const_loss_init
