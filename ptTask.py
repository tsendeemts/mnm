import collections
import argparse
import numpy as np
import functools
import random
import math
import six

class TaskIter(object):

    def __init__(self, max_iter=None, batch_size=1, checkpoint = 100):
        self.max_iter = max_iter
        self.batch_size = batch_size
        self.cur_iter = 0
        self.checkpoint = checkpoint

    def __iter__(self):
        return self

    def __next__(self):
        return self.next()

    def next(self):
        if (self.max_iter is None) or (self.cur_iter < self.max_iter):
            self.cur_iter += 1
            params = self.rand_params()
            return self.gen_example(**params)
        else:
            raise StopIteration()

    def rand_params(self):
        raise NotImplementedError()

    def gen_example(self):
        raise NotImplementedError()
  
class DictInf(TaskIter):

    def __init__(self, size, max_item_length, max_num_items, \
        min_item_length=1, min_num_items=2, max_iter=None, batch_size=1, checkpoint = 100):
        super(DictInf, self).__init__(max_iter=max_iter, batch_size=batch_size, checkpoint=checkpoint)
        self.size = size
        self.batch_size = batch_size
        self.max_item_length = max_item_length
        self.max_num_items = max_num_items
        self.min_item_length = min_item_length
        self.min_num_items = min_num_items
        self.checkpoint = checkpoint
        n_alphabet=26
        self.n_alphabet = n_alphabet + 1
        self.alphabet = np.arange(1, self.n_alphabet, dtype=np.int32)
        self.n_vocab = self.n_alphabet+5

    def rand_params(self, item_length=None, num_items=None):
        if item_length is None:
            item_length = np.random.randint(self.min_item_length, \
                self.max_item_length + 1)
        if num_items is None:
            num_items = np.random.randint(self.min_num_items, \
                self.max_num_items + 1)
        return {'item_length': item_length, 'num_items': num_items}

    def gen_example(self, item_length, num_items):
        alphabet = np.concatenate([np.random.permutation(self.alphabet).reshape((1, -1)) for _ in range(self.batch_size)], axis=0)
        part_ind = int((self.n_alphabet-1)/2)
        alphabet1 = alphabet[:, :part_ind]
        alphabet2 = alphabet[:, part_ind:]

        pair_sep = np.zeros((self.batch_size, 1), np.int32)
        pair_sep[:] = self.n_alphabet+1
        instance_sep = np.zeros((self.batch_size, 1), np.int32)
        instance_sep[:] = self.n_alphabet+2
        pred_sep = np.zeros((self.batch_size, 1), np.int32)
        pred_sep[:] = self.n_alphabet+3
        place_holder = self.n_alphabet+4


        n_max = part_ind
        input_x = []
        inds = []
        for _ in range(num_items):
            ind = np.random.randint(0, n_max, (self.batch_size, item_length))
            src = alphabet1[np.arange(self.batch_size)[:, None], ind]
            trg = alphabet2[np.arange(self.batch_size)[:, None], ind]
            
            input_x.append(src)
            input_x.append(pair_sep)
            input_x.append(trg)
            input_x.append(instance_sep)

            inds.append(ind)
        
        inds = np.concatenate(inds, axis=1)
        ind = np.concatenate([np.random.choice(np.unique(ind), (1, item_length)) for ind in inds], axis=0)

        src = alphabet1[np.arange(self.batch_size)[:, None], ind]
        trg = alphabet2[np.arange(self.batch_size)[:, None], ind]
        pad = np.zeros(trg.shape, dtype=np.int32)
        pad[:] = place_holder

        input_x.append(pred_sep)
        input_x.append(src)
        input_x.append(pair_sep)
        input_x.append(pad)
        
        input_x = np.concatenate(input_x, axis=1)
        output_y = trg

        return input_x, output_y

class DoubleCopyTask(TaskIter):

    def __init__(self, size, max_length, min_length=1, max_iter=None, \
        batch_size=1, checkpoint = 100):
        super(DoubleCopyTask, self).__init__(max_iter=max_iter, batch_size=batch_size, checkpoint=checkpoint)
        self.size = size
        self.min_length = min_length
        self.max_length = max_length
        n_alphabet=10
        self.checkpoint = checkpoint
        self.n_alphabet = n_alphabet + 1
        self.n_vocab = self.n_alphabet + 2

    def rand_params(self, length=None):
        if length is None:
            length = np.random.randint(self.min_length, self.max_length + 1)
        return {'length': length}

    def gen_example(self, length):
        input_x = np.random.randint(1, self.n_alphabet, (self.batch_size, length))
        output_y = np.concatenate([input_x, input_x], axis=1)
        place_holder = np.zeros(input_x.shape, dtype=np.int32)
        place_holder[:] = self.n_alphabet+1
        input_x = np.concatenate([input_x, place_holder], axis=1)

        return input_x, output_y

class AddTask(TaskIter):

    def __init__(self, size, max_length, min_length=1, max_iter=None, \
        batch_size=1, checkpoint = 100):
        super(AddTask, self).__init__(max_iter=max_iter, batch_size=batch_size, checkpoint=checkpoint)
        self.size = size
        self.min_length = min_length
        self.max_length = max_length
        n_alphabet=10
        self.checkpoint = checkpoint
        self.n_alphabet = n_alphabet + 1
        self.n_vocab = self.n_alphabet + 2

    def rand_params(self, length=None):
        if length is None:
            length = np.random.randint(self.min_length, self.max_length + 1)
        return {'length': length}

    def gen_example(self, length):
        input_x = np.random.randint(1, self.n_alphabet, (self.batch_size, length))
        output_y = (input_x[:, :int(length/2)] + np.flip(input_x[:, int(length/2):], axis=1))/2
        output_y = output_y.astype(np.int32)
        place_holder = np.zeros(output_y.shape, dtype=np.int32)
        place_holder[:] = self.n_alphabet+1
        input_x = np.concatenate([input_x, place_holder], axis=1)

        return input_x, output_y