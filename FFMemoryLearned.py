import math
import torch
import torch.nn as nn
import torch.nn.functional as F



class FFMemoryLearned(nn.Module):
   

    def __init__(self, n_in, n_out, n_units = 100, h_nonlinty='tanh', o_nonlinty='identity', read_feat=False):
        super(FFMemoryLearned, self).__init__()
        
        self.l1 = nn.Linear(n_in, n_units).weight.data.cuda()
        self.l2 = nn.Linear(n_units, n_units).weight.data.cuda()
        self.l3 = nn.Linear(n_units, n_out).weight.data.cuda()

        self.targets_net = nn.Linear(n_units, 3*n_units)

        self.feat_proj = nn.Linear(n_units+n_out, n_out)

        self.n_in = n_in
        self.n_units = n_units
        self.n_out = n_out
        
        self.h_nonlinty = h_nonlinty
        self.o_nonlinty = o_nonlinty
        self.read_feat = read_feat

        self.Ws = [self.l1, self.l2, self.l3]

    def unchain_state(self):
        if self.Ws_temp is not None:
          Ws_temp = []
          for W_l in self.Ws_temp:
            Ws_temp.append(W_l.detach())
          self.Ws_temp[:] = Ws_temp

    def reset_mem(self):
        for W_l in self.Ws:
          W_l.detach()
        self.Ws_temp = None

    
    def shape_input(self, x, y=None):
        if len(x.shape) > 2:
          return x, y
        x = x.unsqueeze(dim=1)
        if y is not None:
          y = y.unsqueeze(dim=1)
        return x, y

    def non_linty_pass(self, nonlinty, h):
        if nonlinty == 'tanh':
          return torch.tanh(h)
        elif nonlinty == 'sigmoid':
          return torch.sigmoid(h)
        else:
          return h

    def grad_nonlinty(self, nonlinty, h):
        if nonlinty == 'tanh':
          return 1.0 - h.pow(2)
        elif nonlinty == 'sigmoid':
          return h*(1.0 - h)
        else:
          return 1.0


    def forward(self, x):
        if self.Ws_temp is None:
          self.Ws_temp = []
          for W_l in self.Ws:
            W = W_l.unsqueeze(dim=0).expand((x.shape[0], W_l.shape[0], W_l.shape[1]))
            self.Ws_temp.append(W)
        h_acts = []
        h = x
        for W_l in self.Ws_temp[:-1]:
          h = torch.matmul(h, W_l.transpose(1,2))
          h = self.non_linty_pass(self.h_nonlinty, h)
          h_acts.append(h)
        y_pred = torch.matmul(h, self.Ws_temp[-1].transpose(1,2))
        y_pred = self.non_linty_pass(self.o_nonlinty, y_pred)

        return y_pred, h_acts

    def mse_loss(self, y_pred, y):
        diff = y_pred - y
        diff = diff.view(-1)
        mse = diff.dot(diff)/diff.size()[0]
        return mse

    def read(self, k, weights=None, avg=True):
        k, _ = self.shape_input(k)
        v, h_acts = self.forward(k)
        if self.read_feat:
          v = torch.cat([v, h_acts[-1]], dim=2)
          v = self.feat_proj(v)

        if weights is not None:
          v *= weights
        if avg:
          v = v.mean(dim=1)
        return v

    def update(self, x, y, f_gate=0.1):

        x, y = self.shape_input(x, y)

        y_pred, h_acts = self.forward(x)

        y_pred = y_pred.contiguous()
        y = y.contiguous()
        
        re_const_loss_init = self.mse_loss(y_pred.view(-1, y_pred.shape[2]), y.view(-1, y.shape[2]))

        e = self.targets_net(y.view(-1, y.shape[2]))
        e = e.view(y.shape[0], y.shape[1], -1)
        e = torch.chunk(e, 3, dim=2)
        
        if len(f_gate.shape) < 3:
            f_gate = f_gate.unsqueeze(1)

        h_acts_tail = h_acts + [y_pred]
        h_acts_head = [x] + h_acts
        Ws_t = []
        for W_l, pred, h, target in reversed(list(zip(self.Ws_temp, h_acts_tail, h_acts_head, e))):
            h = h*f_gate.expand(h.shape)
            diff = pred - target
            diff = diff*(2./ (diff.shape[1]*diff.shape[2]))
            W_l = W_l - torch.matmul(diff.transpose(1,2), h)#- 0.0001*W_l 0.1
            Ws_t.insert(0, W_l)

        self.Ws_temp[:] = Ws_t
        
        y_pred, h_acts = self.forward(x)

        re_const_loss = self.mse_loss(y_pred.view(-1, y_pred.shape[2]), y.view(-1, y.shape[2]))
       
        return re_const_loss, re_const_loss_init